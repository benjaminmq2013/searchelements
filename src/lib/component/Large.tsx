import styled from "styled-components"
import { CSSProperties, useState, useEffect } from 'react';

import Input from "@react/rbferwdvroljcgf"
import Card from "@react/tycejxhzhwixoay"

import { largeData, PrizesModel } from '../../data/large';


const Container = styled("div")`
  .card__buttonIcon {
    height: 25px;
    width: 25px;
  }
  .card__icon {
    height: 50px;
    width: 50px;
    padding: 0;
    background-color: initial;
  }
  .card{
    transition: .2s;
    cursor: pointer;
    margin-bottom: 4px;
  }
  .card:hover{
    background-color: #f6f6f9;
  }
  .card__title{
    font-size: 13px;
    font-weight: 700;
  }
`;

export interface params {
  className?: string
  style?: CSSProperties
  onClick?: ()=> void
} 
 
/** 
 * Componente de muestra debes modificar el contenido 
 * @param params 
 * @returns 
 */ 
const App = (params: params): JSX.Element => { 
  params = { ...{ className: 'container' }, ...params } 
 
  const [ value, setValue ] = useState<string>("") 
  const [list, setList] = useState<PrizesModel[]>([]) 

  useEffect(() => {

    setList(largeData.filter(prize => prize.laureates?.filter(person => person.firstname?.toLowerCase().includes(value.toLowerCase()))))

  }, [value])
  
  console.log(list)
 

  return (
    <Container className={ params.className } style={ params.style } >

      <Input placeholder="Search..." type="text" onValue={(value)=> setValue(value)} />     

      {list.map((product, i) => (
        product.laureates && product.laureates.map(person => (
          <Card 
            icon="" 
            buttonIcon="/cart.svg" 
            title={ person.firstname + " " + person.surname }
            subtitle={ product.category } 
            className="card" />
        ))
      ))}     
      

    </Container>

  )
}


export default App