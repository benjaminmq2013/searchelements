import styled from "styled-components"
import { CSSProperties, useState, useEffect } from "react"

import Input from "@react/rbferwdvroljcgf"
import Card from "@react/tycejxhzhwixoay"

import { data } from '../../data/index';
import { ModelProducts } from '../../types/index';


const Container = styled("div")`
  .card__buttonIcon {
    height: 25px;
    width: 25px;
  }
  .card__icon {
    height: 50px;
    width: 50px;
    padding: 0;
    background-color: initial;
  }
  .card{
    transition: .2s;
    cursor: pointer;
    margin-bottom: 4px;
  }
  .card:hover{
    background-color: #f6f6f9;
  }
  .card__title{
    font-size: 13px;
    font-weight: 700;
  }
`;

export interface params {
  className?: string
  style?: CSSProperties
  onClick?: ()=> void
}

/**
 * Componente de muestra debes modificar el contenido
 * @param params 
 * @returns 
 */
const App = (params: params): JSX.Element => {
  params = { ...{ className: 'container' }, ...params }

  const [ value, setValue ] = useState<string>("")
  const [list, setList] = useState<ModelProducts[]>([])

  // Al propósito la peor optimización:
  useEffect(() => {    
    setList(data.filter(prod => {

      // Filters
      if(value !== ""){ 
        return prod.title.toLowerCase().includes(value.toLowerCase())
      }

      // No Filters
      else return prod
    }))
  }, [value])



  console.log(value)

  return (
    <Container className={ params.className } style={ params.style } >

      <Input placeholder="Search..." type="text" onValue={(value)=> setValue(value)} />

      {list.map(product => (
        <Card 
          icon={ product.images ? product.images[0] : "" } 
          buttonIcon="/cart.svg" 
          subtitle={ product.category } 
          title={ product.title }
          className="card" />
      ))}     
      

    </Container>

  )
}


export default App