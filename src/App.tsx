import styled, { createGlobalStyle } from 'styled-components'

// 2 Components:
import Component from "./lib"
import { Large } from './lib'

const Container = styled('div')``

export const GlobalStyle = createGlobalStyle`  
  html,
  body {
    margin: 0px;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  }

  * {
    box-sizing: border-box;
  }
`

const App = () => {

  return (
    <Container>
      <GlobalStyle />
      <Large />
    </Container>
  )
}

export default App
